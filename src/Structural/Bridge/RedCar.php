<?php

namespace Src\Structural\Bridge;

class RedCar implements CarColorInterface
{
    public function getColor()
    {
        return "red";
    }
}
