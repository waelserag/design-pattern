<?php

namespace Src\Structural\Bridge;

class BlueCar implements CarColorInterface
{
    public function getColor()
    {
        return "blue";
    }
}
