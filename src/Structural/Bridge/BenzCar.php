<?php

namespace Src\Structural\Bridge;

class BenzCar extends CarBridge
{
    public function __construct(CarColorInterface $carColor) {
        parent::__construct($carColor);
    }
    public function getCar()
    {
        return "This is Benz car and it has ".$this->carColor->getColor()." color";
    }
}
