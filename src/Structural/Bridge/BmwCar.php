<?php

namespace Src\Structural\Bridge;

class BmwCar extends CarBridge
{
    public function __construct(CarColorInterface $carColor) {
        parent::__construct($carColor);
    }
    public function getCar()
    {
        return "This is BMW car and it has ".$this->carColor->getColor()." color";
    }
}
