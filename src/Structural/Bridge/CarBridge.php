<?php

namespace Src\Structural\Bridge;

abstract class CarBridge
{
    protected $carColor;
    public function __construct(CarColorInterface $carColor) {
        $this->carColor = $carColor;
    }
    abstract function getCar();
}
