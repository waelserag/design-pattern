<?php

namespace Src\Structural\Bridge;

interface CarColorInterface
{
    public function getColor();
}