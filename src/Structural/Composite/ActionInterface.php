<?php

namespace Src\Structural\Composite;

interface ActionInterface
{
    public function add(BoxInterface $product);
    public function remove(BoxInterface $product);
}