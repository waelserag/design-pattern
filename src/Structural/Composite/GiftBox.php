<?php

namespace Src\Structural\Composite;

class GiftBox implements GiftInterface,BoxInterface
{
    public $price;
    public $packagePrice;
    public function __construct(int $price, int $packagePrice) {
       $this->price = $price;
       $this->packagePrice = $packagePrice;
    }
    public function getPackagePrice()
    {
        return $this->packagePrice;
    }

    public function getPrice()
    {
        return $this->price + $this->getPackagePrice();
    }
}
