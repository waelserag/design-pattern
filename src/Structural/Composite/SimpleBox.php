<?php

namespace Src\Structural\Composite;

class SimpleBox implements BoxInterface
{
    public $price;
    public function __construct(int $price) {
       $this->price = $price;
    }
    public function getPrice()
    {
        return $this->price;
    }
}
