<?php

namespace Src\Structural\Composite;

interface GiftInterface
{
    public function getPackagePrice();
}