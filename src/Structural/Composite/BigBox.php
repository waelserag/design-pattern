<?php

namespace Src\Structural\Composite;

class BigBox implements BoxInterface,ActionInterface
{
    public $products;
    public function __construct(array $products) {
       $this->products = $products;
    }
    public function getPrice()
    {
        $total_price = 0;
        foreach ($this->products as $product) {
            $total_price += $product->getPrice();
        };
        return $total_price;
    }

    public function add(BoxInterface $product)
    {
        $this->products[] = $product;
    }

    public function remove(BoxInterface $product)
    {
        $this->products = array_filter($this->products, function($item) use($product){
            if ($item != $product) {
                return $item;
            }
        });
    }
}
