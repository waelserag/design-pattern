<?php

namespace Src\Structural\Composite;

interface BoxInterface
{
    public function getPrice();
}