<?php

namespace Src\Structural\Decorator;

interface PaintingInterface
{
    public function paint(Car $car);
}