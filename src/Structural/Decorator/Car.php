<?php

namespace Src\Structural\Decorator;

class Car
{
    private $color;
    
    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        return $this->color .= $color;
    }
}
