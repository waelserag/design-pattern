<?php

namespace Src\Structural\Adapter1;

class TurboEngine implements TurboInterface
{
    public function startTurbo()
    {
        return "Start Turbo Engine";
    }
}
