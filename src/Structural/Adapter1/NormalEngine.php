<?php

namespace Src\Structural\Adapter1;

class NormalEngine implements EngineInterface
{
    public function startEngine()
    {
        return "Start Normal Engine";
    }
}
