<?php

namespace Src\Structural\Adapter1;

interface EngineInterface
{
    public function startEngine();
}