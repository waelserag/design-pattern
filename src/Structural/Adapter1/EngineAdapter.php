<?php

namespace Src\Structural\Adapter1;

class EngineAdapter implements EngineInterface
{
    private $engine;
    public function __construct(TurboEngine $engine) {
        $this->engine = $engine;
    }
    public function startEngine()
    {
        return $this->engine->startTurbo();
    }
}
