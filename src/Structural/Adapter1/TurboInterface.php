<?php

namespace Src\Structural\Adapter1;

interface TurboInterface
{
    public function startTurbo();
}