<?php

namespace Src\Structural\Adapter2;

interface AuthInterface
{
    public function login(array $params);
}