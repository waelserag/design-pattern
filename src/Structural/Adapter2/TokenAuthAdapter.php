<?php

namespace Src\Structural\Adapter2;

use Admin\TokenAuthLibrary\TokenAuth;

class TokenAuthAdapter implements AuthInterface
{
    private $auth;

    public function __construct(TokenAuth $auth)
    {
        $this->auth = $auth;
    }

    public function login(array $params)
    {
    return $this->auth->tokenLogin($params['token']);
    }
}
