<?php

namespace Src\Structural\Adapter2;

class UserLogin
{
    private $auth;

    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }

    public function login(array $params)
    {
        return $this->auth->login($params);
    }
}
