<?php

namespace Src\Structural\Adapter2;

use Admin\BasicAuthLibrary\BasicAuth;

class BasicAuthAdapter implements AuthInterface
{
    private $auth;

    public function __construct(BasicAuth $auth)
    {
        $this->auth = $auth;
    }

    public function login(array $params)
    {
    return $this->auth->basicLogin($params['useranme'], $params['password']);
    }
}
