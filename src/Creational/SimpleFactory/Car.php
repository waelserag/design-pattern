<?php

namespace Src\Creational\SimpleFactory;

class Car
{
    private $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }
}
