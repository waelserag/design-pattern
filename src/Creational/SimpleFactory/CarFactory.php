<?php

namespace Src\Creational\SimpleFactory;

use Src\Creational\SimpleFactory\Car;

class CarFactory
{
    public function createCar(string $type)
    {
        return new Car($type);
    }
}
