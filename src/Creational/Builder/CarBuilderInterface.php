<?php

namespace Src\Creational\Builder;

use Src\Creational\Builder\Models\Car;

interface CarBuilderInterface
{
    public function createCar();
    public function addwheel();
    public function addBody();
    public function addMotor();
    public function getCar() :Car;
}