<?php

namespace Src\Creational\Builder;

use Src\Creational\Builder\Models\Car;

class Director
{
    private $builder;
     public function __construct(CarBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    public function produceCar() :Car
    {
        $this->builder->createCar();
        $this->builder->addwheel();
        $this->builder->addBody();
        $this->builder->addMotor();
        return $this->builder->getCar();
    }
}
