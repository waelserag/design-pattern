<?php

namespace Src\Creational\Builder;

use Src\Creational\Builder\Models\Car;

class BmwBuilder implements CarBuilderInterface
{
    private $type;

    public function createCar()
    {
        $this->type = new Car();
    }
    public function addwheel()
    {
        $this->type->add("wheel","BMW Wheel");
    }
    public function addBody()
    {
        $this->type->add("body","BMW Body");
    }
    public function addMotor()
    {
        $this->type->add("motor","BMW Motor");
    }
    public function getCar() : Car
    {
        return $this->type;
    }
}
