<?php

namespace Src\Creational\Builder;

use Src\Creational\Builder\Models\Car;

class BenzBuilder implements CarBuilderInterface
{
    private $type;

    public function createCar()
    {
        $this->type = new Car();
    }
    public function addwheel()
    {
        $this->type->add("wheel","Benz Wheel");
    }
    public function addBody()
    {
        $this->type->add("body","Benz Body");
    }
    public function addMotor()
    {
        $this->type->add("motor","Benz Motor");
    }
    public function getCar() : Car
    {
        return $this->type;
    }
}
