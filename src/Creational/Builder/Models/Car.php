<?php

namespace Src\Creational\Builder\Models;

class Car
{
    private $data = [];

    public function add($key, $value)
    {
        $this->data[$key] = $value;
    }
}
