<?php

namespace Src\Creational\StaticFactory;

interface CarInterface
{
    public function build();
}