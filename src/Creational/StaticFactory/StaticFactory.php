<?php

namespace Src\Creational\StaticFactory;

class StaticFactory
{
    public static function factory(string $type)
    {
        if ($type == "BMW") {
            return new BmwCar();
        } elseif ($type == "Benz") {
            return new BenzCar();
        }
        return null;
    }
}
