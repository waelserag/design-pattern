<?php

namespace Src\Creational\StaticFactory;
use Src\Creational\StaticFactory\CarInterface;

class BenzCar implements CarInterface
{
    public function build()
    {
        return "Benz Brand";
    }
}
