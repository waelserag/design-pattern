<?php

namespace Src\Creational\StaticFactory;
use Src\Creational\StaticFactory\CarInterface;

class BmwCar implements CarInterface
{
    public function build()
    {
        return "Bmw Brand";
    }
}
