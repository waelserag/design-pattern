<?php

namespace Src\Creational\Factory;

interface CarBrandInterface
{
    public function createBrand();
}