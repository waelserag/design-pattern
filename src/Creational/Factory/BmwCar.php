<?php

namespace Src\Creational\Factory;
use Src\Creational\Factory\CarBrandInterface;

class BmwCar implements CarBrandInterface
{
    public function createBrand()
    {
        return "Bmw Brand";
    }
}
