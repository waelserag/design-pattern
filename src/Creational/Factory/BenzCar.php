<?php

namespace Src\Creational\Factory;
use Src\Creational\Factory\CarBrandInterface;

class BenzCar implements CarBrandInterface
{
    public function createBrand()
    {
        return "Benz Brand";
    }
}
