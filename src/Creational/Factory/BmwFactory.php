<?php

namespace Src\Creational\Factory;

use Src\Creational\Factory\BrandFactoryInterface;

class BmwFactory implements BrandFactoryInterface
{
    public function build()
    {
        return new BmwCar();
    }
}
