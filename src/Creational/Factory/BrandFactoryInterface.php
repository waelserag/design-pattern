<?php

namespace Src\Creational\Factory;

interface BrandFactoryInterface
{
    public function build();
}