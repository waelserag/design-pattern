<?php

namespace Src\Creational\Factory;

use Src\Creational\Factory\BrandFactoryInterface;

class BenzFactory implements BrandFactoryInterface
{
    public function build()
    {
        return new BenzCar();
    }
}
