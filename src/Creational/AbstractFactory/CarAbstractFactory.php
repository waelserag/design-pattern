<?php

namespace Src\Creational\AbstractFactory;

class CarAbstractFactory
{
    private $price;
    private $tax = 20000;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function createBmwCar()
    {
        return new BmwCar($this->price);
    }
    
    public function createBenzCar()
    {
        return new BenzCar($this->price, $this->tax);
    }
}
