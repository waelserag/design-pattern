<?php

namespace Src\Creational\AbstractFactory;

interface CarInterface
{
    public function calculatePrice();
}