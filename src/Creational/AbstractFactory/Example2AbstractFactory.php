<?php

namespace Src\Creational\AbstractFactory;

class Example2AbstractFactory
{
    private $price;
    private $tax = 20000;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function createCar($type)
    {
        switch ($type) {
            case "bmw":
                return new BmwCar($this->price);
            case "benz":
                return new BenzCar($this->price, $this->tax);
            default:
                return new BmwCar($this->price);
        }
    }
}
