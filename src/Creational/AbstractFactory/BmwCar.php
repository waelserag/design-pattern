<?php

namespace Src\Creational\AbstractFactory;
use Src\Creational\AbstractFactory\CarInterface;

class BmwCar implements CarInterface
{
    private $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function calculatePrice()
    {
        return $this->price + 150000;
    }
}
