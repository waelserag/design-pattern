<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Creational\Factory\BenzCar;
use Src\Creational\Factory\BenzFactory;
use Src\Creational\Factory\BmwCar;
use Src\Creational\Factory\BmwFactory;

class FactoryTest extends TestCase
{
    public function testCanCreateBmwCar()
    {
        $carFactory = new BmwFactory();
        $brand = $carFactory->build();
        $this->assertInstanceOf(BmwCar::class, $brand);
    }

    public function testCanCreateBenzCar()
    {
        $carFactory = new BenzFactory();
        $brand = $carFactory->build();
        $this->assertInstanceOf(BenzCar::class, $brand);
    }

}


