<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Structural\Adapter1\Car;
use Src\Structural\Adapter1\EngineAdapter;
use Src\Structural\Adapter1\NormalEngine;
use Src\Structural\Adapter1\TurboEngine;

class Adapter1Test extends TestCase
{
    public function testCanCreateNormal()
    {
        $engine = new NormalEngine();
        $car = new Car($engine);
        $this->assertEquals("Start Normal Engine", $car->start());
    }

    public function testCanCreateTurboEngine()
    {
        $turbo = new TurboEngine();
        $engine = new EngineAdapter($turbo);
        $car = new Car($engine);
        $this->assertEquals("Start Turbo Engine", $car->start());
    }

}


