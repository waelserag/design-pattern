<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Creational\Builder\BenzBuilder;
use Src\Creational\Builder\BmwBuilder;
use Src\Creational\Builder\Director;
use Src\Creational\Builder\Models\Car;

class BuilderTest extends TestCase
{
    public function testCanCreateBmwCar()
    {
        $builder = new BmwBuilder();
        $director = new Director($builder);
        $myCar = $director->produceCar();
        $this->assertInstanceOf(Car::class, $myCar);
    }

    public function testCanCreateBenzCar()
    {
        $builder = new BenzBuilder();
        $director = new Director($builder);
        $myCar = $director->produceCar();
        $this->assertInstanceOf(Car::class, $myCar);
    }
}


