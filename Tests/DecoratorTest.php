<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Structural\Decorator\BlackPaintingDecorator;
use Src\Structural\Decorator\BluePaintingDecorator;
use Src\Structural\Decorator\Car;
use Src\Structural\Decorator\Painting;
use Src\Structural\Decorator\RedPaintingDecorator;
use Src\Structural\Decorator\YellowPaintingDecorator;

class DecoratorTest extends TestCase
{
    public function testPaintingWithBlackAndBlue()
    {
        $car = new Car();
        $painting = new Painting();
        $painting = new BluePaintingDecorator($painting);
        $painting = new BlackPaintingDecorator($painting);
        $myCar = $painting->paint($car);
        $this->assertEquals("-black--blue-", $myCar->getColor());
    }

    public function testPaintingWithBlackAndBlueAndRedAndYellow()
    {
        $car = new Car();
        $painting = new Painting();
        $painting = new YellowPaintingDecorator($painting);
        $painting = new RedPaintingDecorator($painting);
        $painting = new BluePaintingDecorator($painting);
        $painting = new BlackPaintingDecorator($painting);
        $myCar = $painting->paint($car);
        $this->assertEquals("-black--blue--red--yellow-", $myCar->getColor());
    }

}


