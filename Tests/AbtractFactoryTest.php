<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Creational\AbstractFactory\BenzCar;
use Src\Creational\AbstractFactory\BmwCar;
use Src\Creational\AbstractFactory\CarAbstractFactory;
use Src\Creational\AbstractFactory\Example2AbstractFactory;

class AbstractFactoryTest extends TestCase
{
    public function testCanCreateBmwCar()
    {
        $carFactory = new CarAbstractFactory(50000);
        $bmwCar = $carFactory->createBmwCar();
        $this->assertInstanceOf(BmwCar::class, $bmwCar);
    }

    public function testCanCreateBenzCar()
    {
        $carFactory = new CarAbstractFactory(50000);
        $benzCar = $carFactory->createBenzCar();
        $this->assertInstanceOf(BenzCar::class, $benzCar);
    }

    public function testCanCreateBmwCarExample2()
    {
        $carFactory = new Example2AbstractFactory(50000);
        $bmwCar = $carFactory->createCar("bmw");
        $this->assertInstanceOf(BmwCar::class, $bmwCar);
    }

    public function testCanCreateBenzCarExample2()
    {
        $carFactory = new Example2AbstractFactory(50000);
        $benzCar = $carFactory->createCar("benz");
        $this->assertInstanceOf(BenzCar::class, $benzCar);
    }

    public function testCanCreateDefaultCarExample2()
    {
        $carFactory = new Example2AbstractFactory(50000);
        $defaultCar = $carFactory->createCar("fiat");
        $this->assertInstanceOf(BmwCar::class, $defaultCar);
    }

}


