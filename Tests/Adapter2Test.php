<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Structural\Adapter2\BasicAuthAdapter;
use Src\Structural\Adapter2\TokenAuthAdapter;
use Src\Structural\Adapter2\UserLogin;

class Adapter2Test extends TestCase
{
    public function testLoginWithBasicAuth()
    {
        $auth = $this->createMock(BasicAuthAdapter::class);
         $auth->expects($this->once())
            ->method('login')
            ->willReturn('Wael123456789');
        $userLogin = new UserLogin($auth);
        $userLogin->login([
            'username' => "Wael",
            'password' => '123456789'
        ]);
    }

    public function testLoginWithTokenAuth()
    {
        $auth = $this->createMock(TokenAuthAdapter::class);
         $auth->expects($this->once())
            ->method('login')
            ->willReturn('123456789');
        $login = new UserLogin($auth);
        $login->login([
            'token' => "123456789",
        ]);
    }

}


