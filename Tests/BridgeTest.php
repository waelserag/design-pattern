<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Structural\Bridge\BenzCar;
use Src\Structural\Bridge\BlueCar;
use Src\Structural\Bridge\BmwCar;
use Src\Structural\Bridge\RedCar;

class BridgeTest extends TestCase
{
    public function testBenzRedCar()
    {
        $color = new RedCar();
        $car = new BenzCar($color);
        $this->assertEquals("This is Benz car and it has red color", $car->getCar());
    }
    public function testBenzBlueCar()
    {
        $color = new BlueCar();
        $car = new BenzCar($color);
        $this->assertEquals("This is Benz car and it has blue color", $car->getCar());
    }
    public function testBMWRedCar()
    {
        $color = new RedCar();
        $car = new BmwCar($color);
        $this->assertEquals("This is BMW car and it has red color", $car->getCar());
    }
    public function testBMWBlueCar()
    {
        $color = new BlueCar();
        $car = new BmwCar($color);
        $this->assertEquals("This is BMW car and it has blue color", $car->getCar());
    }

}


