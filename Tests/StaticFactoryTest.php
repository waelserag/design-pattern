<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Creational\StaticFactory\BenzCar;
use Src\Creational\StaticFactory\BmwCar;
use Src\Creational\StaticFactory\StaticFactory;

class StaticFactoryTest extends TestCase
{
    public function testCanCreateBMWCar()
    {
        $carFactory = StaticFactory::factory('BMW');
        $this->assertInstanceOf(BmwCar::class, $carFactory);
    }

    public function testCanCreateBenzCar()
    {
        $carFactory = StaticFactory::factory('Benz');
        $this->assertInstanceOf(BenzCar::class, $carFactory);
    }

}


