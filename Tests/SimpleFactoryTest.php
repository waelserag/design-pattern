<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Creational\SimpleFactory\Car;
use Src\Creational\SimpleFactory\CarFactory;

class SimpleFactoryTest extends TestCase
{
    public function testCanCreateCar()
    {
        $carFactory = new CarFactory();
        $brand = $carFactory->createCar("Kia Sportage");
        $this->assertInstanceOf(Car::class, $brand);
    }

}


