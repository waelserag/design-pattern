<?php

namespace Tests;
use PHPUnit\Framework\TestCase;
use Src\Structural\Composite\BigBox;
use Src\Structural\Composite\SimpleBox;

class CompositeTest extends TestCase
{
    public function testCanCreateBigBoxWithAdd()
    {
        $item1 = new SimpleBox(200);
        $item2 = new SimpleBox(300);
        $total = new BigBox([$item1,$item2]);
        $item3 = new SimpleBox(250);
        $total->add($item3);
        $this->assertEquals(750, $total->getPrice());
    }

    public function testCanCreateBigBoxWithRemove()
    {
        $item1 = new SimpleBox(200);
        $item2 = new SimpleBox(300);
        $total = new BigBox([$item1,$item2]);
        $total->remove($item2);
        $this->assertEquals(200, $total->getPrice());
    }

}


